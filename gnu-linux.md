### Books
1. [Foliate](https://johnfactotum.github.io/foliate/) - A simple, modern & clean E-book reader.

### Web Browser
1. [Mozilla Firefox](https://firefox.org)
2. [LibreWolf](https://librewolf.net) - A fork of Firefox, focused on privacy, security & freedom.


### Utilities
1. [Flameshot](https://flameshot.org) - A modern cross-platform screenshot tool with editing features.
