### App Stores
1. [F-Droid](https://f-droid.org) - a place to find free & open source android apps.
2. [Aurora Store](https://auroraoss.com/) - alternate frontend to PlayStore. Anonymously use playstore with pseudo accounts. [f-droid link](https://f-droid.org/en/packages/com.aurora.store/)

---

### Ad & Tracker Blockers
1. [Blokada](https://blokada.org/) - blocks ads & trackers that spy on you. [f-droid link](https://f-droid.org/en/packages/org.blokada.fem.fdroid/)
2. [TrackerControl](https://trackercontrol.org/) - gives fine grained control over blocking ads & trackers per app.[f-droid link](https://f-droid.org/packages/net.kollnig.missioncontrol.fdroid)

---

### Books
1. [KOReader](https://koreader.rocks/) - PDF & ebook reader. [f-droid link](https://f-droid.org/en/packages/org.koreader.launcher.fdroid/)
2. [Librera](https://librera.mobi/) - document & ebook reader. [f-droid link](https://f-droid.org/en/packages/com.foobnix.pro.pdf.reader/)
3. [Openreads](https://github.com/mateusz-bak/openreads-android) - maintain a list of books you read & rate them. [f-droid link](https://f-droid.org/en/packages/software.mdev.bookstracker/)

---

### Education
1. [Wikipedia](https://wikipedia.org) - the free encyclopedia that anyone can use & improve. [f-droid link](https://f-droid.org/en/packages/org.wikipedia/)

---

### Maps & Navigation
1. [OsmAnd+](https://osmand.net/) - Offline Map & Navigation based on OpenStreetMap data. [f-droid link](https://f-droid.org/en/packages/net.osmand.plus/)

---

### Utilities
1. QR & Barcode Scanner -  QR and barcode scanner with all the features you need. [f-droid link](https://f-droid.org/en/packages/com.example.barcodescanner/)
2. FreeOTP+ - Generate 2FA codes & manage all your 2FA secrets. [f-droid link](https://f-droid.org/en/packages/org.liberty.android.freeotpplus/)
3. Loop Habit Tracker - helps to cultivate good habits & give-up bad habits. [f-droid link](https://f-droid.org/en/packages/org.isoron.uhabits/)

---

### Multimedia
1. [NewPipe](https://newpipe.net) - watch, download & subscribe to YouTube & PeerTube videos. [f-droid link](https://f-droid.org/en/packages/org.schabi.newpipe/)
2. [VLC Media Player](https://videolan.org) - play any video file. [f-droid link](https://f-droid.org/en/packages/com.example.barcodescanner/)
3. [AntennaPod](https://antennapod.org/) - Feature rich Podcast Player. [f-droid link](https://f-droid.org/packages/de.danoeh.antennapod/)

---

### Web Browsers
1. [Firefox](https://firefox.org)
